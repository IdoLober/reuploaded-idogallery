#include "DatabaseAccess.h"
#include <map>

#define ALBUM_ID 0
#define ALBUM_NAME 1
#define CREATION_DATE 2
#define ALBUM_USER_ID 3
#define PICTURE_ID 0
#define TAG_ID 0
#define PICTURE_NAME 1
#define PICTURE_LOCATION 2
#define PICTURE_CREATION_DATE 3
#define PICTURE ID 0
#define USER_NAME 1
#define USER_ID 0

// GLOBALS FOR CALLBACK
bool exist;
sqlite3* db;

int albumsOwnedCounterCallBack(void* data, int argc, char** argv, char** azColName);
int printUsersCallBack(void* data, int argc, char** argv, char** azColName);
int getAlbumIdCallBack(void* data, int argc, char** argv, char** azColName);
int doesAlbumExistCallBack(void* data, int argc, char** argv, char** azColName);
int getAlbumsCallBack(void* data, int argc, char** argv, char** azColName);
int openAlbumCallBack(void* data, int argc, char** argv, char** azColName);
int getAlbumIdCallBack(void* data, int argc, char** argv, char** azColName);
int getTagsOfPicture(void* data, int argc, char** argv, char** azColName);
int getUserCallBack(void* data, int argc, char** argv, char** azColName);
int getSingleIntCallBack(void* data, int argc, char** argv, char** azColName);
int taggedPicturesOfUser(void* data, int argc, char** argv, char** azColName);
int getSingleStringCallBack(void* data, int argc, char** argv, char** azColName);
int countAlbumsTaggedCallBack(void* data, int argc, char** argv, char** azColName);
int doesUserExistCallBack(void* data, int argc, char** argv, char** azColName);


/*
This function will open/create the SQL data base.
Input: None.
Output: result of the procces.
*/
bool DatabaseAccess::open()
{
    std::string dbFileName = "myGallery.sqlite";
    int doesFileExist = _access(dbFileName.c_str(), 0);
    int res = sqlite3_open(dbFileName.c_str(), &db);

    const char* sqlStatement = "CREATE TABLE USERS (ID INTEGER PRIMARY KEY AUTOINCREMENT,NAME TEXT NOT NULL); ";
    char** errMessage = nullptr;

    if (res != SQLITE_OK)
    {
        db = nullptr;
        std::cout << "Failed to open DB 1" << std::endl;
        return false;
    }

    if (doesFileExist != SQLITE_OK)
    {
        // Users table creation:
        res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "Failed to open DB 2" << std::endl;
            return false;

        }

        // Albums table creation:
        sqlStatement = "CREATE TABLE ALBUMS(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL, CREATION_DATE TEXT NOT NULL, USER_ID INTEGER NOT NULL,  FOREIGN KEY(USER_ID) REFERENCES USERS(ID));";
        res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "Failed to open DB 3" << std::endl;
            return false;

        }

        // Pictures table creation:
        sqlStatement = "CREATE TABLE PICTURES(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL, LOCATION TEXT NOT NULL, CREATION_DATE TEXT NOT NULL, ALBUM_ID INTEGER NOT NULL,  FOREIGN KEY(ALBUM_ID) REFERENCES ALBUMS(ID));";
        res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "Failed to open DB 4" << std::endl;
            return false;

        }

        // Tags album creation:
        sqlStatement = "CREATE TABLE TAGS(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,  PICTURE_ID INTEGER NOT NULL, USER_ID INTEGER NOT NULL, FOREIGN KEY(PICTURE_ID) REFERENCES PICTURES(ID));";
        res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
        if (res != SQLITE_OK)
        {
            std::cout << "Failed to open DB 5" << std::endl;
            return false;

        }

    }

    return true;
}
/*
This function will close the data base.
*/
void DatabaseAccess::close()
{
    sqlite3_close(db);
    db = nullptr;
}
void DatabaseAccess::clear()
{
    // Step 1 - clearing the tags:
    std::string sqlStatement = "DELETE FROM TAGS;";
    int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with clearing tags table! (clear function)\n";
    }

    // Step 2 - clearing the pictures:
    sqlStatement = "DELETE FROM PICTURES;";
    res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with clearing the pictures table! (clear function)\n";
    }

    // Step 3 - clearing albums:
    sqlStatement = "DELETE FROM ALBUMS;";
    res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with clearing the albums table! (clear function)\n";
    }

    // Step 4 - clearing users:
    sqlStatement = "DELETE FROM USERS;";
    res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with clearing the users table! (clear function)\n";
    }
}
/*
This function will retrun a list type of object with all the albums in it.
Input: none.
Output: the list object with all the albums in it.
*/
const std::list<Album> DatabaseAccess::getAlbums()
{
    std::list<Album> albumsList;
    std::string sqlStatement = "SELECT * FROM ALBUMS;";
    int res = sqlite3_exec(db, sqlStatement.c_str(), getAlbumsCallBack, &albumsList, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with selecting all the albums! (getAlbums function)\n";
    }

    return albumsList;
}
/*
This function will return all the albums of a given user.
Input: the user.
Output: his albums.
*/
const std::list<Album> DatabaseAccess::getAlbumsOfUser(const User& user)
{
    std::list<Album> albumsList;
    std::string sqlStatement = "SELECT * FROM ALBUMS WHERE USER_ID == " + std::to_string(user.getId()) + ";";
    int res = sqlite3_exec(db, sqlStatement.c_str(), getAlbumsCallBack, &albumsList, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with selecting all the user albums! (getAlbumsOfUser function)\n";
    }

    return albumsList;
}
/*
This function will create an album with the given album information.
Input: the album.
Output. none.
*/
void DatabaseAccess::createAlbum(const Album& album)
{
    
    std::string sqlStatement = "INSERT INTO ALBUMS (NAME, CREATION_DATE, USER_ID) VALUES ('" + album.getName() +"', '" + album.getCreationDate() + "', '" + std::to_string(album.getOwnerId()) + "');";
    int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with creating the album! (createAlbum function)\n";
    }
}
/*
This function will check if a given albums exist.
Input: the album name and the owner id.
Output: true or false.
*/
bool DatabaseAccess::doesAlbumExists(const std::string& albumName, int userId)
{
    exist = false;
    std::string sqlStatement = "SELECT * FROM ALBUMS WHERE USER_ID == " + std::to_string(userId) + ";";
    std::string name = albumName;
    int res = sqlite3_exec(db, sqlStatement.c_str(), doesAlbumExistCallBack, &name, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with selecting all the albums! (getAlbums function)\n";
    }
    return exist;
}
void DatabaseAccess::closeAlbum(Album& pAlbum)
{

}
/*
This function will delete existing album.
Input: the album name and the user id.
Output: none.
*/
void DatabaseAccess::deleteAlbum(const std::string& albumName, int userId)
{
    // Note: prob needs to add part that deletes the tags.

    // Step 1 - need to delete all the pictures from that album in the 'Pictures' table:
    std::string sqlStatement = "DELETE FROM PICTURES WHERE ALBUM_ID == (SELECT ID FROM ALBUMS WHERE NAME == '" + albumName + "');";
    int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with deleting the pictures of the album! (deleteAlbum function)\n";
    }

    // Step 2 - need to delete the album from the 'Albums' table: 
    sqlStatement = "DELETE FROM ALBUMS WHERE NAME == '" + albumName + "' AND USER_ID == " + std::to_string(userId) + ";";
    res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with deleting the album! (deleteAlbum function)\n";
    }
}
/*
This function will open an album from the data base to the Album type variable.
Input: album name.
Output: the album.
*/
Album DatabaseAccess::openAlbum(const std::string& albumName)
{
    int id = 0;
    std::list<Picture> picturesList;
    std::string sqlStatement = "SELECT ID FROM ALBUMS WHERE NAME == '" + albumName + "';";

    int res = sqlite3_exec(db, sqlStatement.c_str(), getAlbumIdCallBack, &id, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with getting the album id! (openAlbum function)\n";
    }
    
    sqlStatement = "SELECT * FROM PICTURES WHERE ALBUM_ID == " + std::to_string(id) + ";";
    res = sqlite3_exec(db, sqlStatement.c_str(), openAlbumCallBack, &picturesList, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with opening the pictures! (openAlbum function)\n";
    }

    Album album(id, albumName);

    for (auto picture : picturesList)
    {
        album.addPicture(picture);
    }
    return album;
}
/*
This function will print all the albums.
Input: none.
Output: none.
*/
void DatabaseAccess::printAlbums()
{
    std::list<Album> albums = getAlbums();
    int i = 0;
    for (auto album : albums)
    {
        i++;
        std::cout << i << " | " << album.getName() << " | " << album.getCreationDate() << " | " << album.getOwnerId() << "\n";
    }
}
/*
This function will print all the users.
Input: none.
Output: none.
*/
void DatabaseAccess::printUsers()
{
    std::string sqlStatement = "SELECT * FROM USERS;";
    int res = sqlite3_exec(db, sqlStatement.c_str(), printUsersCallBack, nullptr, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with getting the album id! (openAlbum function)\n";
    }
}
/*
This function will return a user by his Id.
*/
User DatabaseAccess::getUser(int userId)
{
    std::string sqlStatement = "SELECT * FROM USERS WHERE ID == " + std::to_string(userId) + ";";
    std::string userName = "";

    int res = sqlite3_exec(db, sqlStatement.c_str(), getUserCallBack, &userName, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with selecting the user! (getUser function)\n";
    }

    return User(userId, userName);
}
/*
This function will remove a picture by the album name + the picture name.
Input: the picture's and the album's name.
Output: none.
*/
void DatabaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
    // Deleting the tags of the picture:
    std::string sqlStatement = "DELETE FROM TAGS WHERE PICTURE_ID == (SELECT ID FROM PICTURES WHERE NAME == '" + pictureName + "');";
    int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with deleting the tags of the picture! (removePictureFromAlbumByName function)\n";
    }

    // Deleting the picture himself:
    sqlStatement = "DELETE FROM PICTURES WHERE ALBUM_ID == (SELECT ID FROM ALBUMS WHERE NAME == '" + albumName + "') AND NAME == '" + pictureName + "';";
    res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with deleting the picture! (removePictureFromAlbumByName function)\n";
    }
}
/*
This function will add a picture to a given album name
Input: album name and the picture to add.
Output: none.
*/
void DatabaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
    std::string sqlStatement = "INSERT INTO PICTURES (NAME, LOCATION, CREATION_DATE, ALBUM_ID) VALUES('" + picture.getName() + "', '" + picture.getPath() + "', '" + picture.getCreationDate() + "', (SELECT ID FROM ALBUMS WHERE NAME == '" + albumName + "'));";
    int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with inserting the new picture! (addPictureToAlbumByName function)\n";
    }
}
/*
This function will tag a user in a given album name;
Input: the album name, the picture name and the user id.
Output: none.
*/
void DatabaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
    std::string sqlStatement = "INSERT INTO TAGS (PICTURE_ID, USER_ID) VALUES ((SELECT ID FROM PICTURES WHERE NAME == '" + pictureName + "' AND ALBUM_ID == (SELECT ID FROM ALBUMS WHERE NAME == '" + albumName + "')), " + std::to_string(userId) + ");";
    int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with inserting the tag! (tagUserInPicture function)\n";
    }
}
void DatabaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
    std::string sqlStatement = "DELETE FROM TAGS WHERE PICTURE_ID == (SELECT ID FROM PICTURES WHERE ALBUM_ID == (SELECT ID FROM ALBUMS WHERE NAME == '" + albumName + "') AND NAME == '" + pictureName + "') AND USER_ID == " + std::to_string(userId) + ";";
    int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with deleting the tag! (untagUserInPicture function)\n";
    }
}
/*
This function will insert a user to the 'Users' table.
Input: user object.
Output: none.
*/
void DatabaseAccess::createUser(User& user)
{
    std::string sqlStatement = "INSERT INTO USERS (NAME) VALUES ('" + user.getName() + "');";
    int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with creating the new user! (createUser function)\n";
    }
}
/*
This function will delete a given user.
Input: the user object.
Output: none.
*/
void DatabaseAccess::deleteUser(const User& user)
{
    // Step 1 - need to delete tha tags:
    std::string sqlStatement = "DELETE FROM TAGS WHERE USER_ID == " + std::to_string(user.getId()) + ";";
    int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with deleting the tags of the user! (deleteUser function)\n";
    }

    // Step 2 - need to selete the pictures of the user:
    sqlStatement = "DELETE FROM PICTURES WHERE ALBUM_ID == (SELECT ALBUM_ID FROM ALBUMS WHERE USER_ID == " + std::to_string(user.getId()) + ");";
    res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with deleting the pictures of the user! (deleteUser function)\n";
    }

    // Step 3 - need to delete the albums of the user;
    sqlStatement = "DELETE FROM ALBUMS WHERE USER_ID == " + std::to_string(user.getId()) + ";";
    res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with deleting the albums of the user! (deleteUser function)\n";
    }

    // Step 4 - need to delete the user himself:
    sqlStatement = "DELETE FROM USERS WHERE ID == " + std::to_string(user.getId()) + ";";
    res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with deleting the user! (deleteUser function)\n";
    }

}
/*
This function will check if user exist.
Input: user id
Output: true or false.
*/
bool DatabaseAccess::doesUserExists(int userId)
{
    int id = userId;
    exist = false;
    std::string sqlStatement = "SELECT * FROM USERS;";
    int res = sqlite3_exec(db, sqlStatement.c_str(), doesUserExistCallBack, &id, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with checking if user exist! (doesUserExists function)\n";
    }
    return exist;
}
/*
This function will count the num of albums the the user owned.
Input: the user.
Output: the results.
*/
int DatabaseAccess::countAlbumsOwnedOfUser(const User& user)
{
    int counter = 0;
    std::string sqlStatement = "SELECT COUNT(ID) FROM ALBUMS WHERE USER_ID == " + std::to_string(user.getId()) + ';';
    int res = sqlite3_exec(db, sqlStatement.c_str(), albumsOwnedCounterCallBack, &counter, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with counting the num of albums that the user owned! (countAlbumsOwnedOfUser function)\n";
    }
    return counter;
}
/*
This function will count how much albums does the user taged in.
*/
int DatabaseAccess::countAlbumsTaggedOfUser(const User& user)
{
    std::map<int, std::list<int>> m;
    int userId = user.getId();

    std::string countAlbumsTaggedOfUser = "SELECT * FROM TAGS INNER JOIN PICTURES ON TAGS.PICTURE_ID = PICTURES.ID INNER JOIN ALBUMS ON ALBUMS.ID = PICTURES.ALBUM_ID GROUP BY ALBUMS.ID;";
    int res = sqlite3_exec(db, countAlbumsTaggedOfUser.c_str(), countAlbumsTaggedCallBack, &m, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with counting the num of albums that tagged with the user! (countAlbumsOwnedOfUser function)\n";
    }

    if (m.count(userId) > 0)
    {
        return m[userId].size();
    }

    return 0;

}
/*
This functuon will count total tags of user.
Input: the user.
Output: the num of tags.
*/
int DatabaseAccess::countTagsOfUser(const User& user)
{
    int counter;

    std::string sqlStatement = "SELECT COUNT(USER_ID) FROM TAGS WHERE USER_ID == " + std::to_string(user.getId()) + ';';
    int res = sqlite3_exec(db, sqlStatement.c_str(), getSingleIntCallBack, &counter, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with counting the num of tags that the user have! (countAlbumsOwnedOfUser function)\n";
    }

    return counter;

}
/*
This fucntion will calc the avarage tage of user per album.
Input: the user.
Output: the avg.
*/
float DatabaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
    // if no albums tagged with the user:
    if (countAlbumsTaggedOfUser(user) < 1)
    {
        return 0;
    }

    //  Calculating the avg:
    float res = (float)(countTagsOfUser(user)) / (float)(countAlbumsTaggedOfUser(user));
    return res;

}
/*
This function will return the top tagged user.
Input: none.
Output: the user.
*/
User DatabaseAccess::getTopTaggedUser()
{
    int id = 0;
    std::string username;
    std::string sqlStatement = "SELECT USER_ID FROM TAGS GROUP BY USER_ID ORDER BY COUNT(*) DESC LIMIT 1;";
    int res = sqlite3_exec(db, sqlStatement.c_str(), getSingleIntCallBack, &id, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with looking for the top tagged user! (getTopTaggedUser function)\n";
    }

    sqlStatement = "SELECT NAME FROM USERS WHERE ID = " + std::to_string(id) + ";";
    res = sqlite3_exec(db, sqlStatement.c_str(), getSingleStringCallBack, &username, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with looking for the top tagged user! (getTopTaggedUser function)\n";
    }


    return User(id, username);
}
/*
This function will return the top tagged picture.
Input: none.
Output: the picture.
*/
Picture DatabaseAccess::getTopTaggedPicture()
{
    int pId;
    int userId;
    std::string username;

    std::string sqlStatement = "SELECT PICTURE_ID FROM TAGS GROUP BY PICTURE_ID ORDER BY COUNT(*) DESC LIMIT 1;";
    int res = sqlite3_exec(db, sqlStatement.c_str(), getSingleIntCallBack, &userId, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with looking for the top tagged picture! (getTopTaggedPicture function)\n";
    }
    sqlStatement = "SELECT PICTURE_ID FROM TAGS WHERE USER_ID == " + std::to_string(userId) + ";";
    res = sqlite3_exec(db, sqlStatement.c_str(), getSingleIntCallBack, &pId, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with looking for the top tagged picture! (getTopTaggedPicture function)\n";
    }
    sqlStatement = "SELECT NAME FROM PICTURES WHERE ID == " + std::to_string(pId) + ";";

    res = sqlite3_exec(db, sqlStatement.c_str(), getSingleStringCallBack, &username, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with looking for the top tagged picture! (getTopTaggedPicture function)\n";
    }

    return Picture(pId, username);

}
/*
This function will return all the pictures that the user taged in them.
Input: the user.
Output: the pictures.
*/
std::list<Picture> DatabaseAccess::getTaggedPicturesOfUser(const User& user)
{
    std::list<Picture> picturesList;

    std::string getPictures = "SELECT PICTURES.ID, PICTURES.NAME FROM PICTURES INNER JOIN TAGS ON PICTURES.ID = TAGS.PICTURE_ID WHERE TAGS.USER_ID == " + std::to_string(user.getId()) + ";";
    int res = sqlite3_exec(db, getPictures.c_str(), taggedPicturesOfUser, &picturesList, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with looking for the top tagged picture of user! (getTaggedPicturesOfUser function)\n";
    }

    return picturesList;

}



int getAlbumsCallBack(void* data, int argc, char** argv, char** azColName)
{
    std::list<Album>* albumsList = (std::list<Album>*)data;
    Album album;
    for (int i = 0; i < argc; i++)
    {
        switch (i)
        {
        case ALBUM_NAME:
        {
            album.setName(argv[i]);
            break;
        }
        case ALBUM_USER_ID:
        {
            album.setOwner(std::stoi(argv[i]));
            break;
        }
        case CREATION_DATE:
        {
            album.setCreationDate(argv[i]);
            break;
        }
        }
    }

    albumsList->push_back(album);
    return 0;
}
int doesAlbumExistCallBack(void* data, int argc, char** argv, char** azColName)
{
    std::string* toCheck = (std::string*)data;
    for (int i = 0; i < argc; i++)
    {
        if (i == ALBUM_NAME && argv[i] == *toCheck)
        {
            exist = true;
            return 0;
        }
    }

    exist = false;
    return 0;
}
int openAlbumCallBack(void* data, int argc, char** argv, char** azColName)
{
    std::list<Picture>* picturesList = (std::list<Picture>*)data;
    std::set<int> usersTags;
    int pId = std::stoi(argv[PICTURE_ID]);
    std::string sqlStatement = "SELECT USER_ID FROM TAGS WHERE PICTURE_ID == " + std::to_string(pId) + ";";
    int res = sqlite3_exec(db, sqlStatement.c_str(), &getTagsOfPicture, &usersTags, nullptr);
    if (res != SQLITE_OK)
    {
        std::cout << "Error with getting the id of the tags in the pictures! (openAlbumCallBack function)\n";
    }

    Picture p(std::stoi(argv[PICTURE_ID]), argv[PICTURE_NAME], argv[PICTURE_LOCATION], argv[PICTURE_CREATION_DATE]);
    for (auto tag : usersTags)
    {
        p.tagUser(tag);
    }
    picturesList->push_back(p);
    return 0;
}
int getAlbumIdCallBack(void* data, int argc, char** argv, char** azColName)
{
    int* id = (int*)data;

    if (argc == 0)
    {
        return 0;
    }

    for (int i = 0; i < argc; i++)
    {
        if (i == ALBUM_ID)
        {
            *id = std::stoi(argv[i]);
            return 0;
        }
    }
    return 0;
}
int getTagsOfPicture(void* data, int argc, char** argv, char** azColName)
{
    std::set<int>* usersTags = (std::set<int>*)data;
    usersTags->insert(std::stoi(argv[TAG_ID]));
    return 0;
}
int printUsersCallBack(void* data, int argc, char** argv, char** azColName)
{
    for (int i = 0; i < argc; i++)
    {
        std::cout << azColName[i] << ": "<< argv[i] << "  ";
    }
    std::cout << std::endl;
    return 0;
}
int getUserCallBack(void* data, int argc, char** argv, char** azColName)
{
    std::string* name = (std::string*)data;
    for (int i = 0; i < argc; i++)
    {
        if (i == USER_NAME)
        {
            *name = argv[i];
        }
    }
    return 0;
}
int albumsOwnedCounterCallBack(void* data, int argc, char** argv, char** azColName)
{
    int* counter = (int*)data;
    *counter = std::stoi(argv[0]);
    return 0;
}
int getSingleIntCallBack(void* data, int argc, char** argv, char** azColName)
{
    int* val = (int*)data;
    *val = std::stoi(argv[0]);
    return 0;
}
int getSingleStringCallBack(void* data, int argc, char** argv, char** azColName)
{
    std::string* str = (std::string*)data;
    *str = argv[0];
    return 0;
}
int taggedPicturesOfUser(void* data, int argc, char** argv, char** azColName)
{
    std::list<Picture>* picturesList = (std::list<Picture>*)data;
    picturesList->push_back(Picture(std::stoi(argv[PICTURE_ID]), argv[PICTURE_NAME]));
    return 0;
}
int countAlbumsTaggedCallBack(void* data, int argc, char** argv, char** azColName)
{
    int userId = atoi(argv[2]);
    int albumId = atoi(argv[7]);
    std::map<int, std::list<int>>* counterM = (std::map<int, std::list<int>>*)data;

    if (counterM->count(userId) == 0)
    {
        counterM->insert(std::pair<int, std::list<int>>(userId, std::list<int>()));
    }
    if (std::find(counterM->at(userId).begin(), counterM->at(userId).end(), albumId) == counterM->at(userId).end())
    {
        counterM->at(userId).push_back(albumId);
    }

    return 0;

}
int doesUserExistCallBack(void* data, int argc, char** argv, char** azColName)
{
    int* id = (int*)data;
    for (int i = 0; i < argc; i++)
    {
        if (i == USER_ID && argv[i] == std::to_string(*id))
        {
            exist = true;
            return 0;
        }
    }
    return 0;





}
